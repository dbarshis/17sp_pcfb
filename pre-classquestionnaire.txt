Pre-class questionare for Barshis' Practical Computing for Biologists, ODU, Sp17

1. Why are you taking this class, what do you hope to achieve by the end of the semester?

2. What kind of data from your research do you anticipate needing more advanced computing skills to work with?

3. Rank the following topics according to your interest in covering them during the course (1 is highest interest):
	Text file manipulation
	Basic shell scripting
	python
	working with remote devices (e.g. sensors)
	graphics
	basic operations in R
	advanced operations in R
	web data acquisition
	databases
	cluster computing
	basic bioinformatics for sequence analysis   

4. Describe your computing experience?
	a.) general description?
	b.) have you ever used a unix-based terminal/command line?
	c.) are you familiar with/fluent in any programming languages?
	d.) do you know that python is not just a kind of snake?

5. What OS does your laptop run and what are the basic specs of the machine (e.g. RAM, processors, hard drive space)?

6. Do you have any anticipated conflicts (e.g. research travel, other classes) with the class schedule as it currently stands?
