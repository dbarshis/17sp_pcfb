#! /bin/bash

curl "https://www.wunderground.com/history/airport/ORF/2016/08/[01-31]/DailyHistory.html?&format=1" >> 2016_Aug_ORF_weather.txt
curl "https://www.wunderground.com/history/airport/ORF/2015/08/[01-31]/DailyHistory.html?&format=1" >> 2015_Aug_ORF_weather.txt
curl "https://www.wunderground.com/history/airport/ORF/2014/08/[01-31]/DailyHistory.html?&format=1" >> 2014_Aug_ORF_weather.txt
curl "https://www.wunderground.com/history/airport/ORF/2013/08/[01-31]/DailyHistory.html?&format=1" >> 2013_Aug_ORF_weather.txt
curl "https://www.wunderground.com/history/airport/ORF/2012/08/[01-31]/DailyHistory.html?&format=1" >> 2012_Aug_ORF_weather.txt

echo "Year	PartlyCloudy	80-89.9Temp	90-99.9Temp" >> Barshis_PCfB_Assign5_WeatherOutput.txt
echo "XXXXX" >> Barshis_PCfB_Assign5_WeatherOutput.txt
echo "2016	" >> Barshis_PCfB_Assign5_WeatherOutput.txt
grep -ci "Partly Cloudy" 2016_Aug_ORF_weather.txt >> Barshis_PCfB_Assign5_WeatherOutput.txt
grep -cE "^[^,]+,8[0-9]" 2016_Aug_ORF_weather.txt >> Barshis_PCfB_Assign5_WeatherOutput.txt
grep -cE "^[^,]+,9[0-9]" 2016_Aug_ORF_weather.txt >> Barshis_PCfB_Assign5_WeatherOutput.txt

echo "XXXXX" >> Barshis_PCfB_Assign5_WeatherOutput.txt
echo "2015	" >> Barshis_PCfB_Assign5_WeatherOutput.txt
grep -ci "Partly Cloudy" 2015_Aug_ORF_weather.txt >> Barshis_PCfB_Assign5_WeatherOutput.txt
grep -cE "^[^,]+,8[0-9]" 2015_Aug_ORF_weather.txt >> Barshis_PCfB_Assign5_WeatherOutput.txt
grep -cE "^[^,]+,9[0-9]" 2015_Aug_ORF_weather.txt >> Barshis_PCfB_Assign5_WeatherOutput.txt

echo "XXXXX" >> Barshis_PCfB_Assign5_WeatherOutput.txt
echo "2014	" >> Barshis_PCfB_Assign5_WeatherOutput.txt
grep -ci "Partly Cloudy" 2014_Aug_ORF_weather.txt >> Barshis_PCfB_Assign5_WeatherOutput.txt
grep -cE "^[^,]+,8[0-9]" 2014_Aug_ORF_weather.txt >> Barshis_PCfB_Assign5_WeatherOutput.txt
grep -cE "^[^,]+,9[0-9]" 2014_Aug_ORF_weather.txt >> Barshis_PCfB_Assign5_WeatherOutput.txt

echo "XXXXX" >> Barshis_PCfB_Assign5_WeatherOutput.txt
echo "2013	" >> Barshis_PCfB_Assign5_WeatherOutput.txt
grep -ci "Partly Cloudy" 2013_Aug_ORF_weather.txt >> Barshis_PCfB_Assign5_WeatherOutput.txt
grep -cE "^[^,]+,8[0-9]" 2013_Aug_ORF_weather.txt >> Barshis_PCfB_Assign5_WeatherOutput.txt
grep -cE "^[^,]+,9[0-9]" 2013_Aug_ORF_weather.txt >> Barshis_PCfB_Assign5_WeatherOutput.txt

echo "XXXXX" >> Barshis_PCfB_Assign5_WeatherOutput.txt
echo "2012	" >> Barshis_PCfB_Assign5_WeatherOutput.txt
grep -ci "Partly Cloudy" 2012_Aug_ORF_weather.txt >> Barshis_PCfB_Assign5_WeatherOutput.txt
grep -cE "^[^,]+,8[0-9]" 2012_Aug_ORF_weather.txt >> Barshis_PCfB_Assign5_WeatherOutput.txt
grep -cE "^[^,]+,9[0-9]" 2012_Aug_ORF_weather.txt >> Barshis_PCfB_Assign5_WeatherOutput.txt
echo "XXXXX" >> Barshis_PCfB_Assign5_WeatherOutput.txt
