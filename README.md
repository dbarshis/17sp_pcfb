# Practical Computing for Biologists SP17

This repository contains the source code, datasets, and documentation for 
Dan Barshis' Practical Computing for Biologists course (Biology 701/801) at Old Dominion University, as 
taught in the Spring of 2017.

See the [syllabus](https://bitbucket.org/dbarshis/17sp_pcfb/src/master/syllabus/Biol701-801_PCB_Spring_2017_Barshis.pdf) 
for additional information.

The book for the class is:

Haddock, S. H. D. and Dunn, C. W. (2010). Practical Computing for Biologists. 
[Sinauer Associates](http://practicalcomputing.org). Required.

Here are some of the appendices from the book, which summarize frequently used 
commands:
[Appendices](http://practicalcomputing.org/files/PCfB_Appendices.pdf)

## Pre-class to-do's

  - Fill out pre-class [questionnaire](https://odu.co1.qualtrics.com/SE/?SID=SV_9HzF5EkiWV6biAt) by Tuesday, Feb 28th at 11:59pm
  - Install the software mentioned below
  - Request VPN access asap
  - buy the book
  - run through a basic unix tutorial (google "basic unix tutorial") [this one looks good - do as many as can](http://www.ee.surrey.ac.uk/Teaching/Unix/)
  - windows10 users follow this [link](https://www.howtogeek.com/249966/how-to-install-and-use-the-linux-bash-shell-on-windows-10/) to install the bash terminal  
## Assignments

- Will be posted under Assignments folder

## Software to install on your own computer

A laptop will be required for all in-class sessions. You will use the following programs
to work with data on your laptop.

### For all computers:

- [VPN](https://www.odu.edu/ts/software-services/ciscovpn). Everyone will need to 
register for off-campus vpn access to be able to access the course wiki. Here is
the document explaining how to do it [vpninfo](https://bitbucket.org/dbarshis/17sp_pcfb/src/master/softwareresources/ODU_campus_VPN_access_procedure.pdf). Essentially you'll need to 
request the account access via the [universal account request form](https://bitbucket.org/dbarshis/17sp_pcfb/src/master/softwareresources/universal-account-request-form.pdf), then you'll need
to complete the online safety training, and download the software. If you already 
have access, try and view this web-page 
[pcfbwiki](http://128.82.116.111/dokuwiki/doku.php?id=start), 
it should say "access denied".

- [R](http://www.r-project.org)

### For OS X (ie, Macs):

- [TextWrangler](http://www.barebones.com/products/textwrangler/)
- [Instructions](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) to install Git if it's not already available 

### For Microsoft Windows:

- [Notepad++](http://notepad-plus-plus.org)

- [PuTTY](http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html)

- windows10 users follow this [link](https://www.howtogeek.com/249966/how-to-install-and-use-the-linux-bash-shell-on-windows-10/) to install the bash terminal  

##External links

Documentation and resources for Turing, the computer cluster we may use for our 
analyses:
[Turing](http://www.odu.edu/facultystaff/research/resources/computing/high-performance-computing)

[software-carpentry](http://software-carpentry.org) has some good tutorials and resources
though they are geared more towards two day intensive workshops

[evomics](http://evomics.org/learning/unix-tutorial/) a really good unix tutorial with a slant towards NGS bioinformatics

## Licenses

All original software is licensed under a 
[GPL v3 license](http://www.gnu.org/licenses/gpl-3.0.html). 
Data that is downloaded from public archives is distributed in accordance with 
the license of the source archive. All other original content is distributed 
under a [Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported 
License](http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US).
